package bootstrap

import kotlinx.html.DIV
import kotlinx.html.Tag
import kotlinx.html.TagConsumer
import kotlinx.html.attributesMapOf
import react.RBuilder
import react.ReactElement
import react.dom.RDOMBuilder
import react.dom.tag
import styled.StyledDOMBuilder
import styled.css

inline fun RBuilder.bsContainer(block: RDOMBuilder<DIV>.() -> Unit) : ReactElement = tag(block) { DIV(attributesMapOf("class", "container-fluid"), it) }
inline fun RBuilder.bsRow(block: RDOMBuilder<DIV>.() -> Unit) : ReactElement = tag(block) { DIV(attributesMapOf("class", "row"), it) }
inline fun RBuilder.bsCol(size: String, block: RDOMBuilder<DIV>.() -> Unit) : ReactElement = tag(block) { DIV(attributesMapOf("class", "col-${size}"), it) }

inline fun RBuilder.bsList(block: RDOMBuilder<DIV>.() -> Unit) : ReactElement  = bsTag("list-group",block) { DIV(emptyMap(), it) }
inline fun RBuilder.bsListItem(block: StyledDOMBuilder<DIV>.() -> Unit) : ReactElement = bsTag("list-group-item",block) { DIV(emptyMap(), it) }

inline fun <T : Tag> RBuilder.bsTag(
    bsClasses: String,
    block: StyledDOMBuilder<T>.() -> Unit,
    noinline factory: (TagConsumer<Unit>) -> T
): ReactElement = bsTag(listOf(bsClasses), block, factory)

inline fun <T : Tag> RBuilder.bsTag(
    bsClasses: List<String>,
block: StyledDOMBuilder<T>.() -> Unit,
noinline factory: (TagConsumer<Unit>) -> T
): ReactElement {
    return child(StyledDOMBuilder(factory).apply {
        css {
            classes.addAll(bsClasses)
        }
    }.apply(block).create())
}

