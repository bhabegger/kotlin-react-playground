
import bootstrap.*
import react.*
import react.dom.*

external interface AppState: RState {
    var selectedVideo: Video?
    var unwatchedVideos: List<Video>
    var watchedVideos: List<Video>

}

@JsExport
class App : RComponent<RProps, AppState>() {
    override fun AppState.init() {
        unwatchedVideos = listOf(
            StaticVideo(1, "Building and breaking things", "John Doe", "https://youtu.be/PsaFVLr8t4E"),
            StaticVideo(2, "The development process", "Jane Smith", "https://youtu.be/PsaFVLr8t4E"),
            StaticVideo(3, "The Web 7.0", "Matt Miller", "https://youtu.be/PsaFVLr8t4E")
        )
        watchedVideos = listOf(
            StaticVideo(4, "Mouseless development", "Tom Jerry", "https://youtu.be/PsaFVLr8t4E")
        )
    }

    override fun RBuilder.render() {
        bsContainer {
            h1 { +"KotlinConf Explorer" }
            bsRow {
                bsCol("sm-4") {
                    bsContainer {
                        bsRow {
                            videoList {
                                title = "Videos to watch"
                                videos = state.unwatchedVideos
                                selectedVideo = state.selectedVideo
                                onSelectVideo = {
                                    setState {
                                        selectedVideo = it
                                    }
                                }
                            }
                        }
                        bsRow {
                            videoList {
                                title = "Videos watched"
                                videos = state.watchedVideos
                                selectedVideo = state.selectedVideo
                                onSelectVideo = {
                                    setState {
                                        selectedVideo = it
                                    }
                                }
                            }
                        }
                    }
                }
                bsCol("sm-8") {
                    val selected = state.selectedVideo
                    if(selected != null) {
                        videoPlayer {
                            video = selected
                        }
                    }
                }
            }
        }
    }
}