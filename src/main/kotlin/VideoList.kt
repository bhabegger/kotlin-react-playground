import bootstrap.bsList
import bootstrap.bsListItem
import kotlinx.css.*
import kotlinx.html.js.onClickFunction
import react.*
import styled.*

external interface VideoListProps: RProps {
    var title: String
    var videos: List<Video>
    var selectedVideo: Video?
    var onSelectVideo: (Video) -> Unit
}

@JsExport
class VideoList: RComponent<VideoListProps, RState>() {
    override fun RBuilder.render() {
        styledDiv {
            css {
                classes.add("card")
            }
            styledDiv {
                css {
                    classes.add("card-body")
                }
                styledH5 {
                    css {
                        color = Color.darkBlue
                        fontFamily = "sans-serif"
                    }
                    +props.title
                }
                bsList {
                    for (video in props.videos) {
                        bsListItem{
                            css {
                                if (video == props.selectedVideo) {
                                    classes.add("active")
                                }
                            }
                            attrs {
                                onClickFunction = {
                                    props.onSelectVideo(video)
                                }
                            }
                            key = video.id.toString()
                            +"${video.speaker}: ${video.title}"
                        }
                    }
                }
            }
        }
    }
}

fun RBuilder.videoList(handler: VideoListProps.() -> Unit): ReactElement {
    return child(VideoList::class) {
        this.attrs(handler)
    }
}